﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class RoomChange : MonoBehaviour 
{
    public Room.Type nextRoom;

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var go = GameObject.FindGameObjectWithTag("GameController");
            var gc = go.GetComponent<GameplayController>();
            gc.ChangeRoom(nextRoom);
        }
    }
}
