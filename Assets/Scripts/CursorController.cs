﻿using UnityEngine;
using System.Collections;


public class CursorController : MonoBehaviour 
{
    public Sprite[] cursors;
    private float timer;
    private int index = 0;
    private const float SWITCH_TIME = 2.0f;
    private SpriteRenderer spriteRenderer;


	// Update is called once per frame
	void Start () 
    {
        spriteRenderer = renderer as SpriteRenderer;
        spriteRenderer.sprite = cursors[1];
	}

    void Update()
    {
        Vector3 mouse = Input.mousePosition;
        mouse.z = 10;
        transform.position = Camera.main.ScreenToWorldPoint(mouse);




        timer += Time.deltaTime;

        if (timer >= SWITCH_TIME)
        {
            timer -= SWITCH_TIME;
            index++;
            index = index % cursors.Length;
            spriteRenderer.sprite = cursors[index];
        }
    }
}
