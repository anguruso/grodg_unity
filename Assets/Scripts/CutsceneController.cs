﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cues;

[RequireComponent(typeof(AudioSource))]
public class CutsceneController : MonoBehaviour 
{
    public GUIText subtitles;

    private float timer;
    private const float INTERVAL = 2.0f;
    private List<Cues.Cue> ASO = new List<Cues.Cue>();
    private List<Cues.Cue> CDO = new List<Cues.Cue>();
    private CueVisitor visitor = new CueVisitor();

	// Use this for initialization
	void Start () 
	{
        visitor.guiText = subtitles;
        visitor.audio = audio;

        Vector3 START = new Vector3(11.0f, -3.6672f, 10);
        Vector3 MIDDLE = new Vector3(-2.16f, -3.6672f, 10);
        Vector3 END = new Vector3(-15.0f, -3.6672f, 10);

        //Animation 
        ASO.Add(new Cues.Animation(26, 27, "Sprites/GameObjects/prelude", 1,  START, MIDDLE));
        ASO.Add(new Cues.Animation(30, 32, "Sprites/GameObjects/prelude", 1,  MIDDLE, END));
        ASO.Add(new Cues.Animation(34.0f, 34.5f, "Sprites/GameObjects/F40", 1, START, MIDDLE));

        //Graphics
        ASO.Add(new Graphic(0, 8.5f, "Sprites/Characters/wilsonChair", 0));
        ASO.Add(new Graphic(8.5f, 12.0f, "Sprites/Backgrounds/frontDoor", 0));
        ASO.Add(new Graphic(12.0f, 17.0f, "Sprites/Characters/sonDiss", 0));
        ASO.Add(new Graphic(17.0f, 21.5f, "Sprites/Characters/wilsonClose", 0));
        ASO.Add(new Graphic(21.5f, 26.0f, "Sprites/Characters/sonDiss", 0));
        ASO.Add(new Graphic(26.0f, 50.0f, "Sprites/Backgrounds/meanStreets", 0));
        ASO.Add(new Graphic(27, 30, "Sprites/GameObjects/prelude", 2, MIDDLE));
        ASO.Add(new Graphic(34.5f, 50.0f, "Sprites/GameObjects/F40", 2, MIDDLE));

        //Text
        ASO.Add(new Text(0, 2.5f, "Wilson is playing DodA."));
        ASO.Add(new Text(2.5f, 5.7f, "He is a major pro, and \n already has three kills."));
        ASO.Add(new Text(5.7f, 8.5f, "But then, something terrible happens."));
        ASO.Add(new Text(8.5f, 12.0f, "His DoDA hating Dad comes home from work early."));
        ASO.Add(new Text(12.0f, 17.0f, "Son I am disappoint. I told you to code me a butterfly game."));
        ASO.Add(new Text(17.0f, 21.5f, "But Dad! I'm still downloading the compiler."));
        ASO.Add(new Text(21.5f, 26, "Downloading the compiler my dick! That's the last straw! Come with me."));
        ASO.Add(new Text(26, 30.2f, "So released into the wild, Wilson must now fend for himself."));
        ASO.Add(new Text(30.2f, 35, "What challenges await him? Only time will tell."));

        //Audio
        ASO.Add(new Audio(0, "wilsonDota"));
        ASO.Add(new Audio(2.5f, "somethingTerrible"));
        ASO.Add(new Audio(8.5f, "DoorAttack"));
        ASO.Add(new Audio(12, "codeButterfly"));
        ASO.Add(new Audio(17, "downCompiler"));
        ASO.Add(new Audio(21.5f, "myDick"));
        ASO.Add(new Audio(26, "timeWillTell"));

        //End Cue
        ASO.Add(new EndCue(35));
	}

	// Update is called once per frame
	void Update () 
	{
        timer += Time.deltaTime;
        visitor.timer = timer;

        //Set the timer to the fireAt of the next Audio cue.
        //I.e. Skip to the next Audio Cue.
        if (Input.GetMouseButtonDown(0))
        {
            foreach (Cue c in ASO)
            {
                Audio a = c as Audio;
                EndCue ec = c as EndCue;
                if (a == null && ec == null) continue;

                timer = c.fireAt;
                break;
            }
        }

        //Add cues that need to be added.
        for (int i = 0; i < ASO.Count; i++)
        {
            if (timer > ASO[i].fireAt)
            {
                ASO[i].Fire(visitor);
                CDO.Add(ASO[i]);
                ASO.RemoveAt(i);
            }
        }

        //Update cues that are currently on the scene.
        foreach (Cue cue in CDO)
            cue.OnUpdate(visitor);

        //Remove cues that have expired.
        for (int i = 0; i < CDO.Count; i++)
        {
            if (timer > CDO[i].removeAt)
            {
                CDO[i].OnRemove(visitor);
                CDO.RemoveAt(i);
            }
        }
	}


}