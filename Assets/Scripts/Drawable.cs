// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;


namespace AssemblyCSharp
{
    public class Drawable
    {
        private string spriteName;
        private Vector3 position;
        private Vector3 scale;
        private int drawOrder;
        private GameObject gameObject;

        private bool SetScale { get { return scale != Vector3.zero; }}
        private bool SetPosition { get { return position != Vector3.zero; }}

        public Drawable(string spriteName, int drawOrder, 
            Vector3 position = new Vector3(), Vector3 scale = new Vector3())
        {
            this.spriteName = spriteName;
            this.drawOrder = drawOrder;
            this.position = position;
            this.scale = scale;
        }

        public void Create()
        {
            gameObject = (GameObject)MonoBehaviour.Instantiate
                (Resources.Load<GameObject>(spriteName));
            
            //Set the sort order.
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = drawOrder;
            
            
            if (SetScale)
                gameObject.transform.localScale = scale;
            
            if (SetPosition)
                gameObject.transform.position = position;
        }

        public void Destroy()
        {
            MonoBehaviour.DestroyImmediate(gameObject);
        }
    }
}

