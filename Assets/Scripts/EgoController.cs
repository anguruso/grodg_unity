﻿using UnityEngine;
using System.Collections;

public class EgoController : MonoBehaviour 
{
    public Sprite[] frames;
    public int movesPerFrame;
    public float speed;

    private SpriteRenderer spriteRenderer; 
    private int moves;
    private int currentFrame;

	// Use this for initialization
	void Start () 
    {
        spriteRenderer = renderer as SpriteRenderer;
	}

    public bool Left()  { return Input.GetKey(KeyCode.LeftArrow); }
    public bool Right() { return Input.GetKey(KeyCode.RightArrow); }
    public bool Up()    { return Input.GetKey(KeyCode.UpArrow); }
    public bool Down()  { return Input.GetKey(KeyCode.DownArrow); }
    public bool Movement() { return Left() || Right() || Up() || Down(); }

	// Update is called once per frame
	void Update () 
    {
        float x, y, z;

        //Animate that damn Wilson Cooper.
        if (Movement())
        {
            x = transform.position.x;
            y = transform.position.y;
            z = transform.position.z;

            //Move that damn Wilson Cooper.
            if (Left())
            {
                x -= speed;
                FlipSprite(true);
            }

            else if (Right())
            {
                x += speed;
                FlipSprite(false);
            }
            
            if (Up())
                y += speed; 

            else if (Down())
                y -= speed;

            transform.position = new Vector3(x, y, z);
            UpdateAnimation();
        }
	}

    private void FlipSprite(bool left)
    {
        if (transform.localScale.x > 0 && left) return;
        if (transform.localScale.x < 0 && !left) return;

        transform.localScale = 
            new Vector3(transform.localScale.x * -1, 
                        transform.localScale.y, transform.localScale.z);
    }

    private void UpdateAnimation()
    {
        moves++;
        
        if (moves > movesPerFrame)
        {
            moves = 0;
            currentFrame++;
            currentFrame = currentFrame % frames.Length;
            spriteRenderer.sprite = frames[currentFrame];
        }
    }
}
