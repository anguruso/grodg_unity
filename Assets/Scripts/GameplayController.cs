﻿using UnityEngine;
using System.Collections.Generic;
using AssemblyCSharp;

public class GameplayController : MonoBehaviour 
{
    public GameObject ego;

    private Dictionary<Room.Type, Room> rooms;
    private Room _currentRoom;
    public Room CurrentRoom
    {
        get { return _currentRoom; }
        set 
        { 
            if (_currentRoom != null)
                _currentRoom.Destroy(); 

            _currentRoom = value; 
            _currentRoom.Spawn(); 
        }
    }

    public void ChangeRoom(Room.Type nextRoom)
    {
        Room r = rooms[nextRoom];
        if (r == null) return;

        ego.SetActive(r.ShowWilson);
        CurrentRoom = r;
    }

	// Use this for initialization
	void Start () 
    {
        //Load up the rooms we need.
        rooms = new Dictionary<Room.Type, Room>(20);

        //rooms.Add(Room.Type.MeanStreets1, new Room(Room.Type.MeanStreets1));
        //rooms.Add(Room.Type.MeanStreets2, new Room(Room.Type.MeanStreets2));
        //rooms.Add(Room.Type.OutsideBIFO, new Room(Room.Type.OutsideBIFO));
        //rooms.Add(Room.Type

        AddRoom(Room.Type.MeanStreets1);
        AddRoom(Room.Type.MeanStreets2);
        AddRoom(Room.Type.OutsideBIFO);
        AddRoom(Room.Type.Bakery);
        AddRoom(Room.Type.LanCafe);

        CurrentRoom = rooms[Room.Type.MeanStreets1];
	}

    private void AddRoom(Room.Type type)
    {
        rooms.Add(type, new Room(type));
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (ego.gameObject.transform.position.x <= -9)
        {
            Room nextRoom = rooms[CurrentRoom.west];

            if (nextRoom != null)
            {
                CurrentRoom = nextRoom;

                float y = ego.gameObject.transform.position.y;
                float z = ego.gameObject.transform.position.z;
                ego.gameObject.transform.position = new Vector3(9, y, z);
            }
        }

        else if (ego.gameObject.transform.position .x >= 9)
        {
            Room nextRoom = rooms[CurrentRoom.east];
            
            if (nextRoom != null)
            {
                CurrentRoom = nextRoom;

                float y = ego.gameObject.transform.position.y;
                float z = ego.gameObject.transform.position.z;
                ego.gameObject.transform.position = new Vector3(-9, y, z);
            }
        }
	}
}
