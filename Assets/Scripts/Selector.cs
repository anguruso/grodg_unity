﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour {
	
	private int index;

	// Use this for initialization
	void Start () 
	{
		index = 0;
		SetPosition();
	}

	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.DownArrow)) 
		{
			index = Mathf.Clamp(index + 1, 0, 1);
			SetPosition();
		}

		else if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			index = Mathf.Clamp(index - 1, 0, 1);
			SetPosition();
		}

		else if (Input.GetKeyDown(KeyCode.Return))
		{
			if (index == 0)
			{
				Application.LoadLevel("Cutscene");
			}
			else if (index == 1)
			{	
				Application.Quit();
			}
		}
	}

	//Sets the position based on the index.
	private void SetPosition()
	{
		float x = transform.position.x;
		float z = transform.position.z;

		if (index == 0)
			transform.position = new Vector3(x, 3.25f, z);

		else if (index == 1)
			transform.position = new Vector3(x, 1.75f, z);
	}
}
